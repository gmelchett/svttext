# svttext - Swedish Public Service TeleText on the Console

Shows Swedish Public Service TeleText (in Swedish) on your console. The
data is gathered from https://www.svt.se/svttext/

![alt text](screenshot.png)

The images are converted to look-alike unicode block characters. 


## Usage

```
Syntax ./svttext [page] [options]

page:
 Nothing - shows front page
 Page number(s) or ranges       Show given page. example 100-100 or 100,111
 "d" or "domestic"              Shows domestic news excluding index page(s)
 "n" or "international"         Shows international news excluding index page(s)
 "x" or "index"                 New index
 "s" or "sport"                 Sports
 "e" or "economy"               Economy news
 "r" or "results"               Sport results
 "w" or "weather"               Weather
 "m" or "mix"                   Mixture
 "t" or "tvinfo"                TV-info
 "c" or "content"               TextTV context
 "u" or "ur"                    Utbildningsradion
 "svt1"                         TV guide for SVT1
 "svt2"                         TV guide for SVT2
 "barnkanalen"                  TV guide for Barnkanalen
 "kundskapskanalen"             TV guide for Kundskapskanalen
 "svt24"                        TV guide for SVT24
 "tv3"                          TV guide for TV3
 "tv4"                          TV guide for TV4
 "kanal5"                       TV guide for Kanal 5
 "now"                          What on TV now

options:
 -o <n>                         Max output columns. Default automatically fill
 -h                             This text
```

## TODO
  * A few images on page 700 are not converted to unicode


## Licence

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
