/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2019
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"bitbucket.org/gmelchett/textcolor"
	"fmt"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"io"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
)

const TEXTTV_WIDTH = 41 // Actually size+1
const TEXTTV_HEIGHT = 25

var colorLookup = map[string]int{
	"BK": textcolor.Black,
	"R":  textcolor.Red,
	"G":  textcolor.Green,
	"Y":  textcolor.Yellow,
	"B":  textcolor.Blue,
	"M":  textcolor.Magenta,
	"C":  textcolor.Cyan,
	"W":  textcolor.White,
}

func parseColors(v string, bg int) (int, int) {
	// No idea what appendix "DH" means. Seems pointless.
	colors := strings.Split(strings.Replace(v, " DH", "", -1), " bg")
	if len(colors) == 1 {
		return colorLookup[colors[0]], bg
	} else {
		return colorLookup[colors[0]], colorLookup[colors[1]]
	}
}

/*
 * SVT Texts symbols, looks like this:
 *
 * XX
 * XX
 * XX
 *
 * while the unicode symbols looks like this:
 *
 * XX
 * XX
 *
 * Therefore the graphics looks not that good
 */

var symbolLookup = map[string]string{
	"33": "\u2598",
	"34": "\u259D",
	"35": "\u2580",
	"36": "\u2596",
	"37": "\u2598",
	"38": "\u2580",
	"39": "\u2580",
	"40": "\u2597",
	"41": "G",
	"42": "\u259D",
	"43": "\u258C",
	"44": "\u2501",
	"45": "\u2598",
	"47": "\u2580",
	"48": "\u259D",
	"49": "B",

	"51":  "\u2596",
	"52":  "\u2596",
	"53":  "\u258C",
	"54":  " ",
	"55":  "\u259B",
	"57":  "H",
	"58":  "\u259E",
	"61":  "\u259B",
	"62":  "\u259E",
	"63":  "\u258C",
	"96":  "\u2597",
	"98":  "A",
	"101": "\u2599",
	"102": "F",
	"104": "\u2597",
	"105": "\u2597",
	"106": "\u2590",
	"107": "\u259C",
	"109": "\u259A",
	"110": "\u259C",
	"111": "\u2599",
	"112": "\u2582",
	"114": "\u2580",
	"115": "\u2588",
	"116": "\u2596",
	"117": "\u2596",
	"119": "\u2588",
	"120": "\u2584",
	"122": "\u2590",
	"123": "C",
	"124": "\u2584",
	"125": "\u2599",
	"126": "\u259E",
	"127": "\u2588",
}

type TextTv struct {
	columns int
	cache   map[string][]*Page
}

func parseChars(v string) string {
	m := strings.Replace(v[len(v)-8:len(v)-5], "/", "", -1)
	if _, exists := symbolLookup[m]; !exists {
		fmt.Printf("Symbol: %s has no lookup\n", m)
		return "?"
	}
	return symbolLookup[m]
}

func (tt *TextTv) isCached(page string) (exists bool) {
	_, exists = tt.cache[page]
	return
}

type Page struct {
	next        string
	lines       []string
	links       []string
	complete    bool
	error       bool
	start       bool
	lastPage    bool
	prevDash    bool
	noBroadcast bool
}

func (p *Page) linkExists(link string) bool {
	for i := range p.links {
		if p.links[i] == link {
			return true
		}
	}
	return false
}

func (p *Page) addLink(key, val string) {
	if key != "href" {
		return
	}
	s := strings.Replace(val, "./", "", -1)
	s = strings.Replace(s, ".html", "", -1)

	if _, err := strconv.Atoi(s); err != nil {
		return
	}

	if !p.prevDash {
		if !p.linkExists(s) {
			p.links = append(p.links, s)
		}
		return
	}

	start, _ := strconv.Atoi(p.links[len(p.links)-1])
	if end, err := strconv.Atoi(s); err == nil {
		for i := start; i <= end; i++ {
			l := strconv.Itoa(i)
			if !p.linkExists(l) {
				p.links = append(p.links, l)
			}
		}
	}
}

func (p *Page) parseLine(z *html.Tokenizer) string {
	var line string

	if strings.Count(p.next, "\n") == 0 {
		line = textcolor.Text(textcolor.White, textcolor.Black, p.next)
		p.next = ""
	} else {
		l := strings.SplitN(p.next, "\n", 2)
		p.next = l[1]

		return l[0] + textcolor.Text(textcolor.White, textcolor.Black, " ")
	}
	fg, bg := textcolor.White, textcolor.Black
	nextFg, nextBg := textcolor.White, textcolor.Black

	useSpecialChar := false
	specialChar := " "

out:
	for !p.error {
		tt := z.Next()
		switch tt {
		case html.ErrorToken:
			p.error = true
		case html.StartTagToken:
			t := z.Token()
			switch t.DataAtom {
			case atom.Pre:
				p.start = true
			case atom.A:
				//underline
				if !p.start {
					continue
				}
				line += textcolor.UnderLineOn
				for _, a := range t.Attr {
					p.addLink(a.Key, a.Val)
				}
			case atom.Span:
				if !p.start {
					continue
				}
				useSpecialChar = false
				for _, a := range t.Attr {
					switch a.Key {
					case "class":
						nextFg, nextBg = parseColors(a.Val, bg)
					case "style":
						specialChar = parseChars(a.Val)
						useSpecialChar = true
					default:
						fmt.Println("Unknown key:" + a.Key + " = " + a.Val)
					}
				}
			}
		case html.EndTagToken:
			t := z.Token()
			switch t.DataAtom {
			case atom.A:
				line += textcolor.UnderLineOff
			case atom.Body:
				p.lastPage = true
				break out
			case atom.Pre:
				p.complete = true
				break out
			case atom.Span:
				useSpecialChar = false
			}
		case html.TextToken:
			if !p.start {
				continue
			}
			var s string

			t := z.Token()

			s = html.UnescapeString(t.String())

			if s == "-" {
				p.prevDash = true
			} else {
				p.prevDash = false
			}

			if useSpecialChar {
				s = strings.Replace(s, " ", specialChar, -1)
			}

			l := strings.SplitN(s, "\n", 2)
			if nextFg != fg || nextBg != bg {
				line += textcolor.Color(nextFg, nextBg) + l[0]
				fg = nextFg
				bg = nextBg
			} else {
				line += l[0]
			}
			if len(l) > 1 {
				p.next += l[1]
				break out
			}
		}
	}
	return line + textcolor.Text(textcolor.White, textcolor.Black, " ")
}

func (tt *TextTv) parsePage(h io.Reader) []*Page {

	z := html.NewTokenizer(h)

	pageList := make([]*Page, 0)

outer:
	for {
		page := Page{links: make([]string, 0, 10), lines: make([]string, 0, TEXTTV_HEIGHT), next: ""}

		for {
			l := page.parseLine(z)
			if page.error {
				break
			}
			page.lines = append(page.lines, l)
			if page.complete {
				break
			}
			if page.lastPage {
				break outer
			}
		}

		if len(page.lines) < 10 {
			page.noBroadcast = true
		} else if len(page.lines) < TEXTTV_HEIGHT {
			lp := page.lines[len(page.lines)-1]
			p := page.lines[:len(page.lines)-1]
			for i := 0; i < TEXTTV_HEIGHT-len(page.lines); i++ {
				p = append(p, strings.Repeat(" ", TEXTTV_WIDTH))
			}
			page.lines = append(p, lp)
		}

		pageList = append(pageList, &page)
	}
	return pageList
}

func (tt *TextTv) getPage(pageNr string) {

	if tt.isCached(pageNr) {
		return
	}

	fmt.Printf("Fetching page %s\r", pageNr)

	resp, err := http.Get(fmt.Sprintf("https://www.svt.se/svttext/tv/pages/%s.html", pageNr))
	defer resp.Body.Close()

	if err != nil {
		fmt.Printf("Error: Failed to download and/or parse page: %s\n", pageNr)
		fmt.Println(err)
		os.Exit(1)
	}

	pageList := tt.parsePage(resp.Body)

	tt.cache[pageNr] = pageList
}

func (tt *TextTv) getPages(pages []string) {
	for _, p := range pages {
		tt.getPage(p)
	}
}

func (tt *TextTv) pagesFromRoots(rootPages []string, excludePages []string) []string {
	tt.getPages(rootPages)
	var pagesToShow []string
	for _, p := range rootPages {
		for _, sp := range tt.cache[p][0].links {
			show := true
			for _, n := range excludePages {
				if n == sp {
					show = false
					break
				}
			}
			if show {
				pagesToShow = append(pagesToShow, sp)
			}
		}
	}
	return pagesToShow
}

type Command struct {
	args  []string
	pages string
	help  string
}

var cmdToPages = []Command{
	{args: []string{"x", "index"}, pages: "100-105", help: "New index"},
	{args: []string{"s", "sport"}, pages: "300-302", help: "Sports"},
	{args: []string{"e", "economy"}, pages: "200-202", help: "Economy news"},
	{args: []string{"r", "results"}, pages: "376-395", help: "Sport results"},
	{args: []string{"w", "weather"}, pages: "400", help: "Weather"},
	{args: []string{"m", "mix"}, pages: "500", help: "Mixture"},
	{args: []string{"t", "tvinfo"}, pages: "600", help: "TV-info"},
	{args: []string{"c", "content"}, pages: "700", help: "TextTV context"},
	{args: []string{"u", "ur"}, pages: "800", help: "Utbildningsradion"},
	{args: []string{"svt1"}, pages: "601-603", help: "TV guide for SVT1"},
	{args: []string{"svt2"}, pages: "604-606", help: "TV guide for SVT2"},
	{args: []string{"barnkanalen"}, pages: "611", help: "TV guide for Barnkanalen"},
	{args: []string{"kundskapskanalen"}, pages: "614", help: "TV guide for Kundskapskanalen"},
	{args: []string{"svt24"}, pages: "617", help: "TV guide for SVT24"},
	{args: []string{"tv3"}, pages: "620", help: "TV guide for TV3"},
	{args: []string{"tv4"}, pages: "621", help: "TV guide for TV4"},
	{args: []string{"kanal5"}, pages: "622", help: "TV guide for Kanal 5"},
	{args: []string{"now"}, pages: "650", help: "What on TV now"},
}

func isPageNr(pageNr string) bool {
	if c, err := strconv.Atoi(pageNr); err == nil {
		return c >= 100 && c <= 999
	} else {
		return false
	}
}

func (tt *TextTv) loadPages(pageCmd string) []string {

	var pagesToShow []string

	// Special case
	if pageCmd == "-h" || pageCmd == "--help" {
		help()
	}

outer:
	for i := range cmdToPages {
		for j := range cmdToPages[i].args {
			if cmdToPages[i].args[j] == pageCmd {
				pageCmd = cmdToPages[i].pages
				break outer
			}
		}
	}

	switch pageCmd {
	case "d", "domestic":
		pagesToShow = tt.pagesFromRoots([]string{"101", "102", "103"},
			[]string{"100", "101", "102", "103", "104", "105", "188", "200", "300", "700"})
	case "i", "international":
		pagesToShow = tt.pagesFromRoots([]string{"104", "105"},
			[]string{"100", "101", "102", "103", "104", "105", "188", "200", "300", "700"})
	default:
		if isPageNr(pageCmd) {
			pagesToShow = []string{pageCmd}
		} else {
			for _, p := range strings.Split(pageCmd, ",") {
				if isPageNr(p) {
					pagesToShow = append(pagesToShow, p)
				} else if strings.Count(p, "-") == 1 {
					pageRange := strings.Split(p, "-")
					if isPageNr(pageRange[0]) && isPageNr(pageRange[1]) {
						start, _ := strconv.Atoi(pageRange[0])
						end, _ := strconv.Atoi(pageRange[1])
						for i := start; i <= end; i++ {
							pagesToShow = append(pagesToShow, strconv.Itoa(i))
						}
					}
				} else {
					fmt.Printf("No such page, %s\n", pageCmd)
					os.Exit(1)
				}
			}
		}
	}
	return pagesToShow
}

func help() {
	fmt.Println("\nThis is a console program for showing TeleText from the Swedish Public Service.")
	fmt.Println("The data is gathered from: https://www.svt.se/svttext/\n")
	fmt.Println("Copyright 2019 Jonas Åberg <cja@lithops.se> released under MIT license.\n")
	fmt.Printf("Syntax %s [page] [options]\n\n", os.Args[0])
	fmt.Println("page:")
	fmt.Println(" Nothing - shows front page")
	fmt.Println(" Page number(s) or ranges\tShow given page. example 100-100 or 100,111")

	fmt.Println(" \"d\" or \"domestic\"\t\tShows domestic news excluding index page(s)")
	fmt.Println(" \"n\" or \"international\"\t\tShows international news excluding index page(s)")
	for i := range cmdToPages {
		n := len(cmdToPages[i].args[0]) + 3
		fmt.Printf(" \"%s\"", cmdToPages[i].args[0])
		if len(cmdToPages[i].args) == 2 {
			fmt.Printf(" or \"%s\"", cmdToPages[i].args[1])
			n += len(cmdToPages[i].args[1]) + 6
		}
		fmt.Printf("%s%s\n", strings.Repeat(" ", 32-n), cmdToPages[i].help)
	}

	fmt.Println("\noptions:")
	fmt.Println(" -o <n>\t\t\t\tMax output columns. Default automatically fill to terminal width")
	fmt.Println(" -h\t\t\t\tThis text\n")

	os.Exit(0)
}

func (tt *TextTv) print(pages []string) {
	actualNrPages := 0

	sort.Strings(pages)

	for _, p := range pages {
		for i := 0; i < len(tt.cache[p]); i++ {
			if !tt.cache[p][i].noBroadcast {
				actualNrPages++
			}
		}
	}

	rows := actualNrPages / tt.columns
	if actualNrPages%tt.columns > 0 {
		rows++
	}

	layout := make([][]*Page, rows)
	for i := 0; i < rows; i++ {
		if i+1 != rows {
			layout[i] = make([]*Page, tt.columns)
		} else {
			if actualNrPages%tt.columns == 0 {
				layout[i] = make([]*Page, tt.columns)
			} else {
				layout[i] = make([]*Page, actualNrPages%tt.columns)
			}
		}
	}

	i := 0
	for _, p := range pages {
		for _, sp := range tt.cache[p] {
			if !sp.noBroadcast {
				layout[i/tt.columns][i%tt.columns] = sp
				i++
			}
		}
	}

	for i = 0; i < len(layout); i++ {
		for y := 0; y < len(layout[i][0].lines); y++ {
			for j := 0; j < len(layout[i]); j++ {
				if y < len(layout[i][j].lines) {
					fmt.Printf(layout[i][j].lines[y])
				} else {
					fmt.Printf(strings.Repeat(" ", TEXTTV_WIDTH))
				}
			}
			fmt.Printf("\n")
		}
	}
	fmt.Printf(textcolor.Reset)
}

func main() {

	tt := TextTv{cache: make(map[string][]*Page)}

	if width, _, err := textcolor.GetSize(int(os.Stdin.Fd())); err == nil {
		tt.columns = width / TEXTTV_WIDTH
	} else {
		tt.columns = 80 / TEXTTV_WIDTH
	}

	var pagesToShow []string
	if len(os.Args) == 1 {
		pagesToShow = []string{"100"}
	} else if len(os.Args) >= 3 {
		nextIsColumns := false
		for _, w := range os.Args[2:] {
			switch w {
			case "-h", "--help":
				help()
			case "-o":
				nextIsColumns = true
			default:
				if nextIsColumns {
					if c, err := strconv.Atoi(w); err == nil && c > 0 {
						tt.columns = c
					} else {
						fmt.Printf("%s is not a number, larger than 1\n", w)
						os.Exit(1)
					}
					nextIsColumns = false
				} else {
					fmt.Printf("Unknown option: %s\n", w)
					os.Exit(1)
				}
			}
		}
	}

	if len(os.Args) > 1 {
		pagesToShow = tt.loadPages(os.Args[1])
	}
	tt.getPages(pagesToShow)
	tt.print(pagesToShow)
	os.Exit(0)
}
