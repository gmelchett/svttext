module "https://bitbucket.org/gmelchett/svttext"

go 1.12

require (
	bitbucket.org/gmelchett/textcolor v0.0.0-20190402082142-15ef984cbd66
	github.com/mattn/go-isatty v0.0.7 // indirect
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c
	golang.org/x/net v0.0.0-20190328230028-74de082e2cca
	golang.org/x/sys v0.0.0-20190402054613-e4093980e83e // indirect
)
